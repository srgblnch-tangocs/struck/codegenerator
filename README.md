# codegenerator

![5 - Production](https://img.shields.io/badge/Development_Status-5_--_production-green.svg)

Licence: GPLv3+

This project has been developed in **Max IV Laboratory** by ```antmil``` and 
has received the contribution from **Alba/Cells" by ```sblanch```.
And it has been taken from the [PyNutaqDS](https://git.cells.es/controls/PyNutaqDS) 
project to be used as submodule of other projects that could benefit from 
the features this provides. Further than the original use for the lyrtech 
cards for the Digital Low Level Radio Frequency (DLLRF) and later use with 
Nutaq card, now it is used also by the struck.

release 1.0.15

## Prepare

(...)

## Usage

(...)
