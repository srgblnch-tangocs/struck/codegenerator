#!/usr/bin/env python

# ##############################################################################
#     Code generator for the generation of the device servers.
#
#     Copyright (C) 2013  Max IV Laboratory, Lund Sweden
#     Contribution from Alba Synchrotron/CELLS, Cerdanyola del Valles, Spain
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see [http://www.gnu.org/licenses/].
# ##############################################################################

version = '1.0.15'
