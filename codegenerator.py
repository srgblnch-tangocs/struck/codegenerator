#!/usr/bin/env python

# ##############################################################################
#     Code generator for the generation of the device servers.
#
#     Copyright (C) 2013  Max IV Laboratory, Lund Sweden
#     Contribution from Alba Synchrotron/CELLS, Cerdanyola del Valles, Spain
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see [http://www.gnu.org/licenses/].
# ##############################################################################

__author__ = 'antmil @ MaxIX, sblanch @ Alba'

import os.path

import argparse
import csv
import datetime
import hashlib
from jinja2 import Environment, PackageLoader
from version import version


EXCLUDED_IQ_ATTRIBUTES = []


def create_new_attribute_dict(name, groups, fread):
    """attribute type must be {amp|ph}"""

    if name not in EXCLUDED_IQ_ATTRIBUTES:

        attr = {
            'address': '',
            'name': name,
            'descp': name,
            'access': 'read',
            'type': 'diags',  # RO, or settings for RW
            'group': groups,
            'dtype': 'float',
            'units': '',
            'min_value': '',
            'max_value': '',
            'fwrite': '',
            'fread': fread
        }
        return attr
    else:
        return None


def get_groups_of_an_attribute(attribute_name, attributes_list):
    for attribute_dict in attributes_list:
        if attribute_name == attribute_dict['name']:
            # print("{}".format(attribute_dict))
            groups = attribute_dict['group']
            # print("attribute {} groups {}".format(attribute_name, groups))
            return groups


def get_extended_list_of_attributes(attributes_list):
    print("Extend the csv attribute list with amplitude and phase attributes "
          "for I&Q existing ones")
    # Get list of I,Q attributes and remove the I,Q from the name
    component_attribute_names = [x['name'] for x in attributes_list
                              if (x['name'].startswith('I') or
                                  x['name'].startswith('Q')) and
                                  x['type'] == 'diags']

    component_attribute_suffixes = []
    attribute_groups = {}
    for name in component_attribute_names:
        suffix = name[1:]  # remove the first letter, I or Q
        groups = []
        include_attribute = False
        if name.startswith('I') and \
                "Q{0}".format(suffix) in component_attribute_names:
            include_attribute = True
            attr_grp = get_groups_of_an_attribute(name, attributes_list)
            if isinstance(attr_grp, list):
                groups += attr_grp
        elif name.startswith('Q') and \
                "I{0}".format(suffix) in component_attribute_names:
            include_attribute = True
            attr_grp = get_groups_of_an_attribute(name, attributes_list)
            if isinstance(attr_grp, list):
                groups += attr_grp
        if include_attribute:
            if suffix not in attribute_groups:
                attribute_groups[suffix] = []
            attribute_groups[suffix] += groups
            attribute_groups[suffix] = list(set(attribute_groups[suffix]))
            component_attribute_suffixes.append(suffix)
        else:
            continue
    # remove duplicates
    component_attribute_suffixes = list(set(component_attribute_suffixes))
    
    print("Suffixes of I&Q attributes candidates to have a calculated "
          "Amplitude and Phase: {0}\n".format(component_attribute_suffixes))

    attribute_names = [attribute['name'] for attribute in attributes_list]
    internal_calculated_attributes = []

    if len(component_attribute_suffixes) > 0:
        for name in component_attribute_suffixes:
            amp_attr_name = "Amp{0}".format(name)
            if amp_attr_name not in attribute_names:
                amp_formula = "SQRT(#I{0}#**2+#Q{0}#**2)".format(name)
                amp_group = attribute_groups[name]
                if len(amp_group) == 0:
                    amp_group = ''
                amp_dict = create_new_attribute_dict(
                    amp_attr_name, amp_group, amp_formula)
                internal_calculated_attributes.append(amp_dict)
                print("\tInserted {0}".format(amp_attr_name))
            else:
                print("\tExclude {0} because it is already in the list"
                      "".format(amp_attr_name))
            phase_attr_name = "Phase{0}".format(name)
            if phase_attr_name not in attribute_names:
                phase_formula = "360*ATAN2(#Q{0}#,#I{0}#)/(2*PI)" \
                                "".format(name)
                phase_group = attribute_groups[name]
                if len(phase_group) == 0:
                    phase_group = ''
                ph_dict = create_new_attribute_dict(
                    phase_attr_name, phase_group, phase_formula)
                internal_calculated_attributes.append(ph_dict)
                print("\tInserted {0}".format(phase_attr_name))
            else:
                print("\tExclude {0} because it is already in the list"
                      "".format(phase_attr_name))

    print("{0} internal calculated attributes"
          "".format(len(internal_calculated_attributes)))

    return attributes_list+internal_calculated_attributes


def generate_code(input_filename, output_filename, lyrtech_type, device):

    def _extract_data_from_csv(_input_filename):
        with open(_input_filename, 'r') as file_descriptor:
            attributes_list = list(csv.DictReader(file_descriptor,
                                                  delimiter=';'))
        
        try:
            processed_list = []
            groups = []
            for attr in attributes_list:
                if attr['address'].startswith('#'):
                    continue
                attr['descp'] = attr['name']
                attr['name'] = ''.join(
                    [a for a in attr['name'].replace('-', ' ').split()])
                if len(attr['group']) > 0:
                    attr['group'] = attr['group'].split(',')
                    for group in attr['group']:
                        if group not in groups:
                            groups.append(group)
                processed_list.append(attr)
            print("{} groups ({})".format(len(groups), groups))
        except Exception as exception:
            print("ERROR FOUND IN: {0}\n{1}".format(attr, exception))
                               
        return processed_list
    
    calculated_checksum = hashlib.md5(open(
        input_filename, 'rb').read()).hexdigest()

    cvs_version_file_name = "{0}/version.py".format(
        os.path.dirname(input_filename))
    if os.path.exists(cvs_version_file_name):
        with open(cvs_version_file_name, 'r') as f:
            line = f.readline()
            while len(line) > 0:
                if not line.startswith('#') and line.count('version ='):
                    cvs_version = eval(line.split('version =')[1].strip())
                line = f.readline()
    try:
        cvs_version
    except NameError:
        cvs_version = ""

    # Extract data from csv
    attributes = _extract_data_from_csv(input_filename)
    print("{0} attributes from the csv".format(len(attributes)))

    # Extend list of attributes
    attributes = get_extended_list_of_attributes(attributes)
    print("{0} attributes when extend the list".format(len(attributes)))

    # Prepare environment
    if lyrtech_type.lower() == "gui":
        pkg_loader = PackageLoader('codegenerator', 'templates_gui')
        env = Environment(loader=pkg_loader, trim_blocks=True)
        template = env.get_template('methods.j2')

        # Code Generation
        output = template.render(device=device,
                                 attributes=attributes,
                                 chksum=calculated_checksum,
                                 csvfile=input_filename,
                                 date=str(datetime.datetime.now()),
                                 csv_version=cvs_version,
                                 codegen_version=version)
    else:
        pkg_loader = PackageLoader('codegenerator', 'templates')
        env = Environment(loader=pkg_loader, trim_blocks=True)
        template = env.get_template('methods.j2')

        # Code Generation
        output = template.render(attributes=attributes, 
                                 lyrtech_type=lyrtech_type,
                                 chksum=calculated_checksum,
                                 csvfile=input_filename,
                                 date=str(datetime.datetime.now()),
                                 csv_version=cvs_version,
                                 codegen_version=version)

    # Write files
    fd = open(output_filename, 'w')
    fd.write(output)
    fd.close()
    
    print("\nFile {0} generated successfully with md5 checksum:"
          "\n\n\033[1m{1}\033[0m\n".format(output_filename,
                                           calculated_checksum))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Automatic code generation for DLLRF control")
    # positional arguments
    parser.add_argument(
        'input_filename', help='Input Filename from where to extract the data')
    parser.add_argument(
        'output_filename', help='Filename for generated file')
    # optional arguments
    parser.add_argument(
        '--type', help='Lyrtech type: {loops | settings | diags | all | gui} '
                       '(default all)', default='all')
    parser.add_argument(
        '--device', help='Device name like wr/rf/card-01',
        default='wr/rf/card-01')
    args = parser.parse_args()

    if args.type.lower() == 'gui' and args.device == None:
        print("To create a gui, the device name is required")
    else:
        generate_code(args.input_filename, args.output_filename,
                      args.type.lower(), args.device)
